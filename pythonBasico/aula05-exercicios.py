'''

Exercício: Faça um programa que leia a quantidade de pessoas que serão convidadas para uma festa.
Após isso o programa irá pergunta o nome de todas as pessoas e colocar numa lista de convidados
Após isso irá imprimir todos os nomes da lista.

'''

quantidade_convidados = int(input("Informe o número de convidados: "))
lista_de_convidados = []

while True:
    nome = input("Informe o nome do convidado: ")
    lista_de_convidados.append(nome)
    if len(lista_de_convidados) == quantidade_convidados:
        break

for nome in lista_de_convidados:
    print(nome)

