nomes =["Manoel", "Guilherme", "Marcelo", "Maria", "Júlia"]

# Exemplo do for each
for nome in nomes:
    print(nome)

# podemos exibir uma lista de números dessa forma:

lista_de_numeros = range(5)
for num in lista_de_numeros:
    print(num)

print("")
lista_de_numeros = range(5, 10)
for num in lista_de_numeros:
    print(num)

#Ou então usar esse exemplo do for in range que é similar ao for das linguagens mais antigas
# que significa que o i começa no zero, que o laço vai parar quando o i for igual a sem, e vai incrementar de 2 em 2
for i in range(0, 100, 2):
    print(i)

# também podemos percorrer o laço da seguinte maneira
# que tem o mesmo comportamento do for item in lista
for i in range(len(nomes)):
    print(nomes[i])

# Exibe o índice de cada elemento
for i in range(len(nomes)):
    print(i)

for i in range(len(nomes)):
    print(nomes[i])
    nomes.append("Oiii")

print(nomes)

# outro exemplo de laço o while
i = 0
while i < 10:
    print("i ainda é menor que 10: ", i)
    i += 1

numero = 0
while True:
    print(numero)
    if numero == 20:
        break
    numero += 1




