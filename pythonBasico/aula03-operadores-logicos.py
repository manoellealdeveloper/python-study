# Tipos de dados booleanos
# Utilizados para fazer comparação
var_verdadeiro = True
var_falso = False

print(type(var_verdadeiro), type(var_falso))

if var_verdadeiro == True:
    print("var_verdade é verdadeiro")

if var_falso:
    print("Não é para imprimir")
else:
    print("Essa mensagem deve ser imprimida")

# Operadores de comparação == != > < >= <=

num1 = 10
num2 = 12

if num1 == num2:
    print("Para a expressão num1 == num2, não deve imprimir mensagem, uma vez que o resultado da comparação retorna falso")

if num1 != num2:
    print("Para a expressão num1 != num2, deve imprimir mensagem, uma vez que, o resultado da comparação retorna verdadeiro")

if num1 > num2:
    print("Não deve apresentar essa mensagem")

# Operadores logicos and, or e not

if num1 < num2 and num1 != num2:
    print("Essa mensagem deve ser apresentada")

if num1 > num2 and num1 != num2:
    print("Essa mensagem não deve ser apresentada")

if num1 > num2 or num1 != num2:
    print("Essa mensagem deve ser apresentada")

if num1 > num2 or (not num1 != num2):
    print("Essa mensagem não deve ser apresentada")


# Exemplo elif

print("Opções: \n1 = Escreve Guilherme \n2 = Escreve João \n3 = Escreve Maria")

opcao = input("Escolha uma opção: ")

if opcao == '1':
    print("Guilherme")
elif opcao == '2':
    print("João")
elif opcao == '3':
    print("Maria")
else:
    print("Opção inválida")

