import requests
import json

# www.omdbapi.com

def search_films(search):
    try:
        return requests.get("http://www.omdbapi.com/?s=" + search + "&apikey=414dec0")
    except Exception as err:
        print("Request Error: ", err)

def search_film(id_film):
    try:
        return requests.get("http://www.omdbapi.com/?i=" + id_film + "&apikey=414dec0")
    except Exception as err:
        print("Request Error: ", err)

def print_json_body(request):
    try:
        return json.loads(request.text)
    except Exception as err:
        print("Json Error: ", err)

def print_list_films(list_films):
    try:
        for film in print_json_body(list_films)['Search']:
            film_id = film['imdbID']
            print_film_data(search_film(film_id))
    except KeyError as err:
        print(print_json_body(list_films)['Error'])

def print_film_data(film):
    try:
        print("----------------------------------------------------------------")
        print("Title: ", print_json_body(film)['Title'])
        print("Year: ", print_json_body(film)['Year'])
        print("Director: ", print_json_body(film)['Director'])
        for rating in print_json_body(film)["Ratings"]:
            print("Rating Source: ", rating['Source'])
            print("Rating Value: ", rating['Value'])
    except KeyError as err:
        print(print_json_body(film)['Error'])


exit_command = False
while not exit_command:
    search = input("Search the film or type EXIT for close the program: ")
    if search == "EXIT":
        exit_command = True
    else:
        list_films = search_films(search)
        print_list_films(list_films)
