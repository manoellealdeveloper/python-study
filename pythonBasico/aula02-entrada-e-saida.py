'''
Aula 2 - Entrada e saida de dados e operações matemáticas básicas

'''


# Imprimindo valores no console

print("Hello World!")
print("Segundo print")
print("Olá mundo!\nTestando a quebra de linha em python!")

# Variáveis e concatenação de strings

nome = "Manoel"
idade = 28
altura = 1.72
tipo_nome = str(type(nome))
tipo_idade = str(type(idade))
tipo_altura = type(altura)
print("Nome: " + nome)
print("Idade: " + str(idade))
print(altura)
print("A variável nome é do tipo: " + tipo_nome)
print("A variável idade é do tipo:", tipo_idade)
print(tipo_altura)

# Operações matemáticas

numero1 = 28
numero2 = 45
resultado = numero1 + numero2

print(resultado)

resultado_pontencia = 4 ** 2
resultado_raiz = 4 ** (1/2)

print("Resultado potência:", resultado_pontencia)
print("Resultado raiz quadrada:", resultado_raiz)

# Input

nome = input("Escreva seu nome: ")
idade = input("Escreva sua idade: ")
altura = input("Escreva sua altura: ")

# Quando se usa a vírgula, não precisa converter as variáveis em string para serem passadas como argumento no print()
print(nome, "tem", idade, "anos e", altura, "de altura")