# Lista (list) - É mutável
minha_lista = ['Gui', 'João']

# Tupla (tuple) - Não é mutável, ou seja, não e possível incluir, alterar ou excluir elementos da tupla
minha_tupla =('Gui', 'João')

# Dicionário (dict) - Composto por chave e valor, similar a um Json ou o tipo Hash do Java
meu_dicionario = {'nome': 'Manoel', 'idade': 27}

# Conjunto (set) - Não é possível ter itens repetidos dentro de um conjunto, não possui índice e nem e ordenado
meu_conjunto = {'Manoel', 'João'}


# funções da tupla
print(minha_tupla)
print(type(minha_tupla))
print(minha_tupla[0])

if 'Gui' in minha_tupla:
    print("Gui está na minha coleção")

# Funções em um dicionário
print(meu_dicionario)
print(meu_dicionario['nome'])
print(type(meu_dicionario))

if "Manoel" in meu_dicionario.values():
    print("Manoel esta dentro do dicionário")

if "nome" in meu_dicionario.keys():
    print("O dicionário possui a chave nome")

for valores in meu_dicionario.values():
    print(valores)

meu_dicionario['nome'] = "João"
print(meu_dicionario)

# incluir uma chave e valor em um dicionário
meu_dicionario['Endereço'] = 'Av. João das Neves'
print(meu_dicionario)

# Funcões em um conjunto - Conjunto e bem mais performático em questão de busca
meu_conjunto.add("Maria")

print(meu_conjunto)
