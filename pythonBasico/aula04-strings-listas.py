frase = "Oi, tudo bem?"

print(frase)

print("No Python as Strings se comporta como lista de char, mas não confunda, são tipos de dados diferentes")
print(frase[0])
print(frase[0:5])
print("Tudo em caixa baixa:", frase.lower())
print("Tudo em caixa alta:", frase.upper())
print("Separa a frase de acordo com o delimitador passado como parâmetro:", frase.split(","))

lista_nomes = ['João', 'Maria', 'Guilherme', 'Diego']

print(lista_nomes)
print(lista_nomes[1])
print("Podemos definir o intervalo dos itens que eu quero conforme abaixo")
print(lista_nomes[0:2])
print("Podemos também informar um número negativo como índice da lista, dessa forma, a lista vai ser percorrida de trás para frente")
print(lista_nomes[-1])
print("Podemos incluir itens na lista conforme abaixo")
lista_nomes.append("Geralda")
print("Podemos remover também itens na lista conforme abaixo")
lista_nomes.remove("João")
print(lista_nomes)
print("Podemos inserir um elemento em uma posição expecífica da lista")
lista_nomes.insert(1, "Robervánia")
print(lista_nomes)
print("Podemos também verificar quantos elementos com um determinado valor tem numa lista")
print(lista_nomes.count("Robervánia"))
print("Retornar o tamanho da lista: ", lista_nomes.__len__())
print("Existem vários outros métodos para trabalhar com a lista, como o método abaixo para limpar a lista")
lista_nomes.clear()
print(lista_nomes)

lista = ["Manoel", 28, True, ["Brasilia", "Df"]]

print ("Em Python, uma lista pode conter vários tipos de dados diferentes, conforme: ", lista)