import requests
# Beautiful Soup 4 BS4

def realizar_requisicao(url):
    try:
        return requests.get(url)
    except Exception as err:
        print("Requisicao deu Erro: ", err)

def imprimir_dados_requisicao(requisicao):
    try:
        print(type(requisicao))
        print(requisicao)
        print(requisicao.status_code)
        print(requisicao.text)
    except Exception as err:
        print("Algum erro ao imprimir dados da requisição: ", err)

requisicao1 = realizar_requisicao("https://g1.com.br")
imprimir_dados_requisicao(requisicao1)

requisicao2 = realizar_requisicao("https://www.google.com")
imprimir_dados_requisicao(requisicao2)

cabecalho = {"User-Agent":"Windows 12",
             "Referer":"https://google.com.br"}

try:
    requests.get("https://putsreq.com/PsZ0KxeGTaYzLooRV4nf", cabecalho)
except Exception as err:
    print("Requisicao deu Erro: ", err)

