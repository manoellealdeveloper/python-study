'''
Exercício: Escreva uma função que recebe um objeto de coleção e retorna o valor do maio número dentro dessa coleção.
Faça outra função que retorna o menor número dessa coleção
'''

def maior_numero(lista):
    aux = 0
    for numero in lista:
        if numero > aux:
            aux = numero
    return aux

def menor_numero(lista):
    aux = maior_numero(lista)
    for numero in lista:
        if numero < aux:
            aux = numero
    return aux

lista_de_numeros = []

print("Escreva números para serem incluídos na lista")

while True:
    numero = input("Digite um número: ")
    lista_de_numeros.append(int(numero))
    sair_laco = input("Deseja digitar mais números? (Sim/S ou Não/N): ")
    if sair_laco == "Não" or sair_laco == "N" or sair_laco == "não" or sair_laco == "Nao" or sair_laco == "nao":
        break

print(str(maior_numero(lista_de_numeros)))
print(str(menor_numero(lista_de_numeros)))








