'''
Exercício: Faça um formulário que pergunte o nome, cpf, endereço, idade, altura e telefone
E imprima isso num relatório organizado
'''

nome = input("Informe o nome: ")
cpf = input("Informe o cpf: ")
endereco = input("Informe o endereço: ")
idade = input("Informe a idade: ")
altura = input("Informe a altura: ")
telefone = input("Informe o telefone: ")

print("Nome: ", nome,"\nCPF:", cpf, "\nEndereco:", endereco, "\nIdade:", idade,
      "\nAltura: ", altura, "\nTelefone:", telefone)