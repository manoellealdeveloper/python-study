import requests
import json

# www.omdbapi.com

def search_film(film_name):
    try:
        return requests.get("http://www.omdbapi.com/?t=" + film_name + "&apikey=414dec0")
    except Exception as err:
        print("Request error: ", err)

def print_json_body(film_data):
    try:
        return json.loads(film_data.text)
    except Exception as err:
        print("Json body Error: ", err)

def print_film_data(film_data):
    try:
        print(print_json_body(film_data))
        print("Title: ", print_json_body(film_data)['Title'])
        print("Year: ", print_json_body(film_data)['Year'])
        print("Director: ", print_json_body(film_data)['Director'])
        for rating in print_json_body(film_data)["Ratings"]:
            print("Rating Source: ", rating['Source'])
            print("Rating Value: ", rating['Value'])
    except KeyError as err:
        print(print_json_body(film_data)['Error'])

exit_program = False

while not exit_program:
    film_name = input("Type a film name or type EXIT for close the program: ")
    if film_name == "EXIT" or film_name == "Exit" or film_name == "exit":
        exit_program = True
    else:
        film_data = search_film(film_name)
        print_film_data(film_data)
