'''
Exercício: Faça um programa que pergunte a idade, o peso e a altura de uma pessoa e decide se ela está apta a ser do
Exército.
Para entrar no Exército é preciso ter mais de 18 anos, pesar ao menos 60 quilos, e altura maior ou igual a 1.70 metros
'''

print("Formulário Exército")
idade = int(input("Digite sua idade: "))
peso = float(input("Digite seu peso: "))
altura = float(input("Digite sua altura: "))

if idade >= 18 and peso >= 60 and altura >= 1.70:
    print("Parabéns, você tem todos requisitos para entrar no Exército")
else:
    print("Que pena, você não atendeu algum(s) do(s) requisito(s) necessários")


