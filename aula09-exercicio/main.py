from cliente import Cliente
from conta import Conta

'''
Exercício: Crie um software de gerenciamento bancário
Esse software poderá ser capaz de criar clientes e contas
Cada cliente possui nome, cpf, idade
Cada conta possui um cliente, saldo, limite, sacar, depositar e consultar saldo

'''

manoel = Cliente("Manoel", "01234567890", 28)
josi = Cliente("Jôsi", "00000192832", 25)

conta01 = Conta(manoel, 100.00, 10.00)
conta02 = Conta(josi, 780.00, 50.00)

# Cliente Manoel
print(conta01.cliente.nome)
print(conta01.cliente.cpf)
print(str(conta01.cliente.idade))
print(conta01.saldo)
conta01.sacar(111.00)
conta01.depositar(500.00)
conta01.sacar(111.00)
print(conta01.consultar_saldo())

# Cliente Jôsi
print(josi)