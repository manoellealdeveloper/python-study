class Conta:

    def __init__(self, cliente, saldo, limite):
        self.cliente = cliente
        self.saldo = saldo
        self.limite = limite

    def sacar(self, valor):
        if valor > self.saldo + self.limite:
            print("Erro: Saldo insulficiente")
        else:
            self.saldo -= valor

    def depositar(self, valor):
        self.saldo += valor

    def consultar_saldo(self):
        return self.saldo
