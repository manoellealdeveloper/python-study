# de dentro do arquivo veículo, importando a classe Veiculo
from veiculo import Veiculo
from carro import Carro

caminhao_vermelho = Veiculo("Vermelho", 6, "Ford", 10)

print(type(caminhao_vermelho))
print(caminhao_vermelho.cor)
print(caminhao_vermelho.rodas)
print(caminhao_vermelho.marca)
print(caminhao_vermelho.tanque)

carro_azul = Carro("Azul", 4, "BMW", 30)
print(carro_azul.cor)
print(carro_azul.marca)
print(carro_azul.tanque)

carro_azul.abastecer(20)

print(carro_azul.tanque)
